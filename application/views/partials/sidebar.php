<div class="navbar-default sidebar" role="navigation" style="overflow: visible;">
            <div class="slimScrollDiv" style="position: relative; overflow: visible; width: auto; height: 100%;"><div class="sidebar-nav slimscrollsidebar" style="overflow: visible hidden; width: auto; height: 100%;">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navegacion</span></h3>
                </div>
                <ul class="nav in" id="side-menu">
                    <li style="padding: 70px 0 0;">
                        <a href="<?php echo base_url();?>index.php/inventory" class="waves-effect active"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Inventario</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/observaciones" class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Observaciones</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/recuento" class="waves-effect"><i class="fa fa-user fa-fw" aria-hidden="true"></i>Recuento</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/ventas" class="waves-effect"><i class="fa fa-table fa-fw" aria-hidden="true"></i>Ventas</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/busqueda" class="waves-effect"><i class="fa fa-font fa-fw" aria-hidden="true"></i>Busqueda</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/historialC" class="waves-effect"><i class="fa fa-columns fa-fw" aria-hidden="true"></i>Historial vacuno</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/clients" class="waves-effect"><i class="fa fa-info-circle fa-fw" aria-hidden="true"></i>Agenda ganaderos</a>
                    </li>

                </ul>
            </div><div class="slimScrollBar" style="background: rgba(0, 0, 0, 0.3); width: 6px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 950px;"></div><div class="slimScrollRail" style="width: 6px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>

        </div>
